#!/bin/bash

CPUSET=$1



function start() {
    #../nsjail/nsjail --chroot / -- /home/daniel/nsjail_test/cpuset_test "$1" 10 cpuset_appl #> log/$1.log
    #../nsjail/nsjail --use_cgroupv2 --cgroup_cpuset $CPUSET --chroot / -- /home/daniel/nsjail_test/cpuset_test "$1" 10 cpuset_appl #> log/$1.log
    #../nsjail/nsjail --use_cgroupv2 --cgroup_cpu_weight $2 --chroot / -- /home/daniel/nsjail_test/cpuset_test "$1" 10 cpuset_appl #> log/$1.log
    ../nsjail/nsjail -t 10 --use_cgroupv2 --cgroup_cpu_weight $2 --cgroup_cpuset $CPUSET --chroot / -- /home/daniel/nsjail_test/cpuset_test "$1" 20 cpuset_appl #> log/$1.log
}

mkdir -p log
start "A"     200 200 &
start "B "    200 300 &
start "C  "   200 400 &
start "D   "  200 500 &
start "E    " 200 600 &

sleep 12
