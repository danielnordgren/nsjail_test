#
# Monitor cgroups, path to all groups as input arguments
# Example: sudo python3 cgroups_monitor.py /sys/fs/cgroup/NS*
#

import sys
from time import sleep, time_ns

class Group:

    def __init__(self, path):
        self.path = path
        self.stats = None
        self.updated = False
        self.usage = 0
        self.diff = dict()
        self.last = time_ns() / 1000
        self.cpuset = None

    def read_value(self, attr):
        f = open(self.path + "/" + attr)
        for line in f:
            w = line.strip().split()
            if len(w) > 0:
                return w[0]
        return None
        
    def alive(self):
        return self.updated
    
    def set_value(self, attr, value):
        print ("Set " + attr + " to " + value)
        f = open(self.path + "/" + attr, "w")
        f.write(value)
    
    def update(self):
        now = time_ns() / 1000
        tdiff = now - self.last
        self.last = now
        f = open(self.path + "/cpu.stat")
        new_stats = dict()
        for line in f:
            i, t = line.strip().split()
            new_stats[i] = int(t)
            

        if self.stats is not None:
            if new_stats != self.stats:
                self.updated = True
            for s in self.stats:
                self.diff[s] = 1000 * (new_stats[s] - self.stats[s]) / tdiff

        self.stats = new_stats
                
        self.weight = int(self.read_value("cpu.weight"))
        self.nice   = self.read_value("cpu.weight.nice")
        self.cpuset = self.read_value("cpuset.cpus")
        self.cpuset = self.read_value("cpuset.cpus.effective")
        self.cpusetp = self.read_value("cpuset.cpus.partition")
        if not self.updated:
            self.weight = 0

    def print1(self, weights_sum):
        if self.updated:
            if "-" in self.cpuset:
                first,last = map(int, self.cpuset.split("-"))
                num_cpus = last-first+1
            elif "," in self.cpuset:
                num_cpus = len(self.cpuset.split(","))
            else:
                num_cpus =1
            rel_weight = 1.0 * self.weight / weights_sum
            usage = int(self.diff['usage_usec'])
            calc_usage = 1000*num_cpus*rel_weight
            print(f"MONITOR: {self.last/1000000:20.2f} {self.path} {self.weight} {self.cpuset} {usage} {num_cpus} {rel_weight} {calc_usage} {100*abs(usage-calc_usage)/calc_usage}")
            self.updated = False

timeout = int(sys.argv[1])
period = float(sys.argv[2])
groups = [Group(g) for g in sys.argv[3:]]

t = 0

cpusets = { 10 : "2",
            15 : "1,2",
            20 : "1,2,3",
            25 : "2,3"}

weights = { 30 : [100, 100, 100, 4000, 100],
            35 : [100, 100, 400, 400, 100]}

while True:
    weights_sum = 0
    for g in groups:
        g.update()
        weights_sum += g.weight
    
    t += period

    if t > timeout:
        exit(0)

    if int(t) in cpusets:
        cpus = cpusets[int(t)]
        for g in groups:
            g.set_value("cpuset.cpus", cpus)
        del cpusets[int(t)]
        
    if int(t) in weights:
        weight = weights[int(t)]
        index = 0
        for g in groups:
            g.set_value("cpu.weight", str(weight[index]))
            index += 1
        del weights[int(t)]
        
    for g in groups:
        g.print1(weights_sum)

    sleep(period)
    
