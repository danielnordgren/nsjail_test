CC=gcc

cpuset_test: cpuset_test.c
	$(CC) $< -o $@ -lpthread 

contain: contain.c
	$(CC) $< -o $@

all : cpuset_test contain

run_once : all
	sudo ./cpuset_test.sh 1,2

clean :
	rm cpuset_test contain
