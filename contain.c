#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

static char cgroup_path[100];
	
int write_int(char* attr, int value) {
  char path[100], buf[10];
  sprintf(path, "%s/%s", cgroup_path, attr);
  FILE* file = fopen(path, "w");
  sprintf(buf, "%d", value);
  fwrite(buf, strlen(buf), 1, file);
  fclose(file);
}

int write_str(char* attr, char* value) {
  char path[100];
  sprintf(path, "%s/%s", cgroup_path, attr);
  FILE* file = fopen(path, "w");
  fwrite(value, strlen(value), 1, file);
  fclose(file);
}

int main(int argc, char** argv) {
  sprintf(cgroup_path, "/sys/fs/cgroup/Appl_%s", argv[2]);
  mkdir(cgroup_path, 0700);
  
  write_int("cgroup.procs", getpid());
  write_str("cpuset.cpus", "1,2");
  execve(argv[1], &argv[1], NULL);
 

}
