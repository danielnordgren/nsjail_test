#!/bin/bash


function start() {
    name=$1
    weight=$2
    shift
    shift
    ./cgroups_container.sh $name $weight -n $name $@
}


start A 100 -b 10 -w 1  $@ &

start B 200 -b 10 -w 1 $@ &

start C 300 -b 10 -w 1  $@ &

for i in $(seq $1)
do
    start X$i 100 -b 0 -w 1 $@ &
done
#start F -b 4 -w 4 -f 4 $@ &

read

pkill cpuset_test

sleep 2

rmdir /sys/fs/cgroup/test_cgroup_*
