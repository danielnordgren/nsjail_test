
#define _GNU_SOURCE             /* See feature_test_macros(7) */
#include <sched.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>


typedef struct {
  char      name[64];
  pthread_t thread;
  clockid_t clock_id;
  bool      fifo;
} thread_info_t;

static char* name = "A";
static int num_busy_threads = 0;
static int num_fifo_threads = 0;
static int num_wait_threads = 0;
static int num_threads;
static int fifo_core = -1;
static thread_info_t *threads;
static int verbose = 0;

static int sockfd;
static struct sockaddr_in servaddr;

static long long nsec(clockid_t clock_id) { 
  
  struct timespec ts;
  clock_gettime(clock_id, &ts);
  return ts.tv_sec * 1000000000 + ts.tv_nsec;
  
}

static long long wait_until(long long t) { 
  struct timespec ts = {t / 1000000000, t % 1000000000};
  clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &ts, NULL);
}

static void set_affinity(bool fifo) {
  
    cpu_set_t set;

    if (fifo_core < 0) {
      return;
    }
    
    if (fifo) {
      CPU_ZERO(&set);
      CPU_SET(fifo_core, &set);
    } else {
      if (sched_getaffinity(0, sizeof(set), &set) == -1) {
	perror("getaffinity failed");
	exit(EXIT_FAILURE);
      }
      
      CPU_CLR(fifo_core, &set);
    }
    
    if (sched_setaffinity(0, sizeof(set), &set) == -1) {
      perror("setaffinity failed");
      exit(EXIT_FAILURE);
    }
}

static void work() {

  long data[ 10000 ];
  int i;
  for (i = 0; i < 5000; i++) {
    data[ i ] = 2 * data[ 9999 - i];
  }
    
}

static long long get_nsec() { 
  
  return nsec(CLOCK_MONOTONIC);
  
}

static long long get_thread_time() { 
  
  return nsec(CLOCK_THREAD_CPUTIME_ID);
  
}

static void * busy_start(void *arg)
{
  set_affinity(false);
  
  while(1)
  {
    work();
  }
}

static void * wait_start(void *arg)
{
  long long work_ms   =   2;
  long long period_ms = 200;

  long long next      = get_nsec();
  long long next_work = get_thread_time();
  set_affinity(false);
  
  while(1)
  {
    int i;
    next_work += work_ms * 1000000;
    while (get_thread_time() < next_work) {
      work();
    }
    next += period_ms * 1000000;
    wait_until(next);
  }
}

static void * fifo_start(void *arg)
{
  long long work_duration_ms  = 10;
  long long sleep_duration_ms = 100;
  struct sched_param param = (struct sched_param) {.sched_priority = 10};
  pthread_setschedparam(pthread_self(), SCHED_FIFO, &param);
  set_affinity(true);
  while(1)
  {
    int i;
    long long next = get_nsec() + work_duration_ms * 1000000;
    while (get_nsec() < next) {
      work();
    }
    usleep(sleep_duration_ms * 1000);
  }
}

static void send_buffer(char* buffer, int buffer_size){

    if (verbose) {
      printf(buffer);
    }
    sendto(sockfd, buffer, buffer_size,
	   0, (const struct sockaddr *) &servaddr, 
	   sizeof(servaddr));
    
}

static void * monitor_start(void *arg)
{
  int i;
  struct timespec ts = {1,0};
  char buffer[ 1024 ];
  int buffer_size;
  long long last = 0, last_cpu = 0;

  if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
    perror("socket creation failed");
    exit(EXIT_FAILURE);
  }
  
  memset(&servaddr, 0, sizeof(servaddr));
  
  // Filling server information
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(5005);
  servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
      
  while(1) {
    nanosleep(&ts, NULL);
    
    long long now = nsec(CLOCK_MONOTONIC);
    long long cpu = nsec(CLOCK_PROCESS_CPUTIME_ID);
    long long realtime = nsec(CLOCK_REALTIME);
    long long fifo_usage = 0;

    if (last != 0) {
      printf("CPU_LOAD %ld %s %d %%\n", now, name, (100 * (cpu-last_cpu)) / (now-last));
    }
    last_cpu = cpu;
    last = now;
    buffer_size = sprintf(buffer, "%s Process %llu %llu\n",
			  name, cpu, realtime);
    
    send_buffer(buffer, buffer_size);
    
    for (i = 0; i < num_threads; i++) {
      thread_info_t* thread_p = &threads[ i ];
      long long usage = nsec(thread_p->clock_id);
      
      buffer_size = sprintf(buffer, "%s %s %llu %llu\n", name, thread_p->name, usage, realtime);
      send_buffer(buffer, buffer_size);
    
      if (thread_p->fifo) {
	fifo_usage += usage;
      }
      
    }
    buffer_size = sprintf(buffer, "%s Async %llu %llu\n",
			  name, cpu - fifo_usage, realtime);
    send_buffer(buffer, buffer_size);
    
    buffer_size = sprintf(buffer, "%s FIFO %llu %llu\n",
			  name, fifo_usage, realtime);
    send_buffer(buffer, buffer_size);
    
    
  }
}

int argparse(int argc, char** argv) {
  int opt;
  
  while ((opt = getopt(argc, argv, "n:f:F:b:w:v")) != -1) {
    switch (opt) {
    case 'n':
      name = optarg;
      break;
    case 'f':
      num_fifo_threads = atoi(optarg);
      break;
    case 'F':
      fifo_core = atoi(optarg);
      break;
    case 'b':
      num_busy_threads = atoi(optarg);
      break;
    case 'w':
      num_wait_threads = atoi(optarg);
      break;
    case 'v':
      verbose = 1;
      break;
    default: /* '?' */
      fprintf(stderr, "Usage: %s [-f num-fifo] [-F fifo-core] [-b num-busy] [-w num-wait] [-v]\n",
	      argv[0]);
      exit(EXIT_FAILURE);
    }
  }
}

int main(int argc, char** argv) {
  int i;
  char buf[100];
  thread_info_t *thread_p;
  
  argparse(argc, argv);
  
  printf("Starting %d FIFO threads, %d busy work threads and %d wait-work threads\n",
	 num_fifo_threads, num_busy_threads, num_wait_threads);

  num_threads = 1 + num_fifo_threads + num_busy_threads + num_wait_threads;
  
  threads = calloc(num_threads, sizeof(thread_info_t));
  
  thread_p = &threads[0];
  
  for(i = 0; i < num_busy_threads; i++) {
    pthread_create(&thread_p->thread, NULL,
		   &busy_start, NULL);
    sprintf(thread_p->name, "Busy_%d", i);
    thread_p++;
  }

  for(i = 0; i < num_wait_threads; i++) {
    pthread_create(&thread_p->thread, NULL,
		   &wait_start, NULL);
    sprintf(thread_p->name, "Wait_%d", i);
    thread_p++;
  }

  for(i = 0; i < num_fifo_threads; i++) {
    pthread_create(&thread_p->thread, NULL,
		   &fifo_start, NULL);
    sprintf(thread_p->name, "Fifo_%d", i);
    thread_p->fifo = true;
    thread_p++;
  }

  pthread_create(&thread_p->thread, NULL,
		 &monitor_start, NULL);
  sprintf(thread_p->name, "Monitor");

  for(i = 0; i < num_threads; i++) {
    pthread_getcpuclockid(threads[i].thread, &threads[i].clock_id);
  }
  
  for(i = 0; i < num_threads; i++) {
    pthread_join(threads[i].thread, NULL);
  }
  

}
