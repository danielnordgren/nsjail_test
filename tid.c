#define _GNU_SOURCE
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/nsfs.h>
#include <signal.h>
#include <pthread.h>

static void pid_info() {
  printf("My tid: %d pid: %d ppid: %d\n", gettid(), getpid(), getppid());

}

static void * thread_start(void *arg)
{
  pid_info();
  while(1)
  {
    sleep(1);
  }
}

void handle_sig(int sig) {
  printf("Got sig: %d\n", sig);
  pid_info();
}

int main()
{
  int num_threads = 5;
  pthread_t threads[ num_threads ];
  int i;

  
  signal(SIGTERM, handle_sig);
  fork();
  fork();
  pid_info();
  sleep(1);
  for(i = 0; i < num_threads; i++) {
    pthread_create(&threads[i], NULL,
		   &thread_start, NULL);
    sleep(1);

  }


  for(i = 0; i < num_threads; i++) {
    pthread_join(threads[i], NULL);
  }
}
