#include <stdio.h>
#include <stdlib.h>
#include <dirent.h> 
#include <string.h> 
#include <signal.h> 

int tid_map[ 100 ] = { 0 };

int get_ns_pid(int pid, int tid) {
  
  char  proc_name[ 100 ];
  snprintf(proc_name, sizeof proc_name, "/proc/%d/task/%d/status", pid, tid);
  FILE* proc_file = fopen(proc_name, "r");
  char name[ 100 ];
  
  while (1) {
    int proc_tid = 0, proc_nstid = 0;
    int ret = fscanf(proc_file, "%s %d %d", &name, &proc_tid, &proc_nstid);
    if (strcmp(name, "NSpid:") == 0) {
      return proc_nstid;
    }
  }
  fclose(proc_file);
  return 0;
}

void print_map(int pid) {
  char  proc_name[ 100 ];
  snprintf(proc_name, sizeof proc_name, "/proc/%d/task/%d/children", pid, pid);
  FILE* proc_file = fopen(proc_name, "r");
  int child;
  if (proc_name == NULL) {
    printf("Unable to open file\n");
    exit(1);
  }
  while (fscanf(proc_file, "%d", &child) == 1) {
    printf("Child: %d\n", child);
    char  dir_name[ 100 ];
    snprintf(dir_name, sizeof dir_name, "/proc/%d/task", child);

    DIR *d;
    struct dirent *dir;
    d = opendir(dir_name);
    if (d) {
      while ((dir = readdir(d)) != NULL) {
	if (dir->d_name[0] != '.') {
	  int tid = atoi(dir->d_name);
	  int nstid = get_ns_pid(child, tid);
	  printf("Tid: %d => %d\n", tid, nstid);
	  tid_map[ nstid ] = tid;
	}
      }
      closedir(d);
  }
    
    print_map(child);
  }
  fclose(proc_file);
}

int main(int argc, char** argv) {

  int ppid = atoi(argv[1]);

  printf("Parent pid = %d\n", ppid);

  print_map(ppid);

  while (1) {
    int tid;
    printf("tid to signal: ");  
    scanf("%d", &tid);
    kill(tid_map[tid], SIGTERM);

  }
  
}
