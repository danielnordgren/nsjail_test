#!/bin/bash


function test() {
    ./cpuset_test.sh $1 &
    sleep 2
    python3 cgroups_monitor.py 10 2 /sys/fs/cgroup/NSJAIL*
}

for j in 1 2 3 4 5
do
    for i in 0 1 2 3 "0,1" "1,2" "2,3" "1,2,3"
    do
	test $i
	sleep 1
    done
done


