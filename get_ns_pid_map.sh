top_pid=$1

for cpid in `cat /proc/$top_pid/task/$top_pid/children`
do
    echo $cpid
    $0 $cpid
    grep NSpid /proc/$cpid/task/*/status
done
