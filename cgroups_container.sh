#!/bin/bash

PID=$$
echo $PID
name=$1
weight=$2
shift
shift
CG_PATH=/sys/fs/cgroup/test_cgroup_$name
echo $CG_PATH $weight
mkdir -p $CG_PATH
echo $PID > $CG_PATH/cgroup.procs
echo $weight > $CG_PATH/cpu.weight
echo "2-3" > $CG_PATH/cpuset.cpus
./cpuset_test $@
